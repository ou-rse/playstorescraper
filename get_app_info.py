import urlparse
from datetime import datetime
from lxml import html

import pandas as pd
import requests

KEYWORDS = ['weight', 'sleep', 'steps', 'walking', 'timekeeping',
            'time keeping', 'heartrate', 'work outs',
            'heart rate', 'running', 'spending', 'purchases', 'body fat',
            'eating', 'diet', 'supplements', 'exercise', 'mood',
            'happiness', 'income', 'workouts', 'cycling']

NOW = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
OUTPUT = "android_apps.{0}.csv".format(NOW)


def get_html(url):
    """
    Requests the web page @ url and returns a lxml tree representation of the
    html.
    Parameters
    ----------
    url: url to a web page

    Returns
    -------
    a tree representation of the html
    """
    page = requests.get(url)
    tree = html.fromstring(page.text)

    return tree


def get_rating(tree):
    """
    Gets the average rating of a play store app.
    Parameters
    ----------
    tree: lxml tree representation of a app play store html page.

    Returns
    -------
    float: The average rating of the app.
    """
    xpath_text = "//meta[@itemprop='ratingValue']/@content"

    try:
        rating = tree.xpath(xpath_text)[0]
    except IndexError:
        rating = 0

    rating = float(rating)

    return rating


def get_total_ratings(tree):
    """
    Gets the total ratings count for a playstore app.
    Parameters
    ----------
    tree: lxml tree representation of a app play store html page.

    Returns
    -------
    int: total ratings count (how many times the app has been rated)
    """
    xpath_text = "//meta[@itemprop='ratingCount']/@content"

    try:
        total_ratings = tree.xpath(xpath_text)[0]
    except IndexError:
        total_ratings = 0

    total_ratings = int(total_ratings)

    return total_ratings


def last_updated(tree):
    """
    Gets the date the play store app was updated.
    Parameters
    ----------
    tree: lxml tree representation of a app play store html page.

    Returns
    -------
    string: date the app was last updated.
    """
    xpath_text = "//div[@itemprop='datePublished']/text()"

    return tree.xpath(xpath_text)[0]


def get_downloads(tree):
    """
    Range of for number of downloads of the app.
    Parameters
    ----------
    tree: lxml tree representation of a app play store html page.

    Returns
    -------
    dict: {upper: int, lower: int} range for how often the app was downloaded.
    """
    downloads = dict()
    xpath_text = "//div[@itemprop='numDownloads']/text()"
    text = tree.xpath(xpath_text)[0]
    values = text.split(" ")

    downloads['lower'] = int(values[1].replace(',', ''))
    downloads['upper'] = int(values[3].replace(',', ''))

    return downloads


def get_app_name(tree):
    """
    Get the name of the app.
    Parameters
    ----------
    tree: lxml tree representation of a app play store html page.

    Returns
    -------
    string: the app name.
    """
    xpath_text = "//div[@class='id-app-title']/text()"
    text = tree.xpath(xpath_text)[0]

    return text


def get_genre(tree):
    """
    Get the play store genre of the app.
    Parameters
    ----------
    tree: lxml tree representation of a app play store html page.

    Returns
    -------
    string: the app genre
    """
    xpath_text = "//span[@itemprop='genre']/text()"
    genre = tree.xpath(xpath_text)[0]

    return genre


def play_store_search(term):
    """
    Get the html of the results page when a keyword search is performed using
    the keyword [term].
    Parameters
    ----------
    term: term used to perform the search.

    Returns
    -------
    lxml tree representation of a search results play store html page.
    """
    url = "https://play.google.com/store/search?q={0}&c=apps".format(term)
    tree = get_html(url)
    return tree


def get_app_urls(tree):
    """
    Get all the app urls in the page.
    Parameters
    ----------
    tree: Tree representation of the google play store search results page.

    Returns
    -------
    [url, url, url]: A list of url strings of the apps contained on the search
    page.
    """
    xpath_text = "//a[@class='title']/@href"
    urls = tree.xpath(xpath_text)
    return urls


def date_parse(date_str, fmt="%d %B %Y"):
    """
    Date parser for date format used by play store
    Parameters
    ----------
    date_str: date string to be parsed
    fmt: date format to be used.

    Returns
    -------
    pandas datetime object
    """
    return pd.datetime.strptime(date_str, fmt)


def get_app_details(url, keywords):
    """
    Gets a tree representation of the app page @ the url and extracts the meta
    data for the app.
    Parameters
    ----------
    url: url pointing to a play store app.
    keywords: search keywords that returned that url

    Returns
    -------
    dict containing the meta data for the app.
    """
    tld = "https://play.google.com"
    app_url = tld + url
    print("url: {}".format(app_url))
    tree = get_html(app_url)

    details = dict()

    # Get the app name
    name = get_app_name(tree)
    print("name: {0}".format(name.encode('utf-8')))
    details['name'] = name

    # Get the app rating.
    details['rating'] = get_rating(tree)

    # Get the unique app id.
    details['id'] = urlparse.parse_qs(urlparse.urlparse(url).query)['id'][0]

    # Get the download boundaries.
    downloads = get_downloads(tree)
    details['lowerDownloads'] = downloads['lower']
    details['upperDownloads'] = downloads['upper']

    # Get the date the app as last updated.
    details['last_updated'] = date_parse(last_updated(tree))

    # Get the total ratings for the app (the number of times the app has been
    # rated
    details['total_ratings'] = get_total_ratings(tree)

    # Get the app genre
    details['genre'] = get_genre(tree)

    # Add the keywords to the details dictionary
    details['keywords'] = keywords

    return details


def fetch_app_urls():
    """
    Performs a google play store search for each of the keywords in KEYWORDS.
    Adds the url to a dict where the key is the unique url and the value is
    a list of keywords that caused that url to be returned.
    Returns
    -------
    app_urls: {url1: list(keyword1, keyword2), url2: list(keyword1, keyword3)}
    """
    app_urls = dict()
    for keyword in KEYWORDS:
        print("Fetching data for {}".format(keyword))
        html_tree = play_store_search(keyword)
        for url in get_app_urls(html_tree):
            keyword_list = app_urls.get(url, list())
            keyword_list.append(keyword)
            app_urls[url] = keyword_list

    print("Keyword search finished.")
    print("{} apps found.".format(len(app_urls.values())))

    return app_urls


def process_app(app_urls):
    """
    Process all the urls and fetch meta data and save details to a csv file.
    Parameters
    ----------
    app_urls: dict of {urls: list(keywords)}

    Returns
    -------
    None
    """

    # List that will hold the app details dict
    app_details = list()

    # iterate over each app and get the app meta data
    total_app_count = len(app_urls.values())
    for app_url, keywords in app_urls.iteritems():
        print("Processing app {} of {}".format(
            len(app_details) + 1, total_app_count))
        meta_data = get_app_details(app_url, keywords)
        app_details.append(meta_data)
        print("--------------")

    # Create pandas DataFrame
    df = pd.DataFrame()
    df = df.from_dict(app_details)

    # Filter the apps to only include apps from approved genres
    # this excludes game apps and other genres
    df = df[df.genre.isin(['Health & Fitness', 'Lifestyle', 'Medical',
                           'Finance', 'Sports', 'Productivity', 'Tools',
                           'Business'])]

    # Save to file.
    print('Saving data to csv')
    df.to_csv(OUTPUT, encoding='utf-8')


def run():
    app_urls = fetch_app_urls()
    process_app(app_urls)

if __name__ == '__main__':
    run()
